# My Resume & Coverletter pipeline

`Requires: make, pdftex, slmenu, pdfunite`

## Steps:
- Add coverletter recipients to the `recipients` folder following the template.
- Then run `make`, and select the recipient (the selection uses slmenu).
- The output folder will include the build coverletter, resume, and then the pdfunited package.
